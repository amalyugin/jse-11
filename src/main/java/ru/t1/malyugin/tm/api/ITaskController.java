package ru.t1.malyugin.tm.api;

public interface ITaskController {

    void showTasks();

    void showTaskById();

    void showTaskByIndex();

    void createTask();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

}