package ru.t1.malyugin.tm.api;

import ru.t1.malyugin.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}