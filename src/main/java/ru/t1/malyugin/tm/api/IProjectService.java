package ru.t1.malyugin.tm.api;

import ru.t1.malyugin.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

}