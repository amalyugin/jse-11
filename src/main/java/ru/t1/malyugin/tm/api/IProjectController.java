package ru.t1.malyugin.tm.api;

public interface IProjectController {

    void showProjects();

    void showProjectById();

    void showProjectByIndex();

    void createProject();

    void clearProjects();

    void removeProjectById();

    void removeProjectByIndex();

    void updateProjectById();

    void updateProjectByIndex();

}