package ru.t1.malyugin.tm.controller;

import ru.t1.malyugin.tm.api.IProjectController;
import ru.t1.malyugin.tm.api.IProjectService;
import ru.t1.malyugin.tm.model.Project;
import ru.t1.malyugin.tm.util.TerminalUtil;

import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showProjects() {
        System.out.println("[SHOW PROJECTS]");

        final List<Project> projects = projectService.findAll();
        if (projects.isEmpty()) {
            System.out.println("[EMPTY]");
            return;
        }
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project.toString());
            index++;
        }
        System.out.println("[OK]");
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECTS BY ID]");

        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("[PROJECT NOT FOUND]");
            return;
        }
        System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECTS BY INDEX]");

        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextInteger() - 1;
        final Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("[PROJECT NOT FOUND]");
            return;
        }
        System.out.println(project);
        System.out.println("[OK]");
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");

        System.out.print("ENTER NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();
        final Project project = projectService.create(name, description);
        if (project == null) {
            System.out.println("[FAILED]");
            return;
        }

        System.out.println("NEW PROJECT - " + project.toString());
        System.out.println("[OK]");
    }

    @Override
    public void clearProjects() {
        System.out.println("[REMOVE PROJECTS]");
        projectService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT BY ID]");

        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.removeById(id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT BY INDEX]");

        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextInteger() - 1;
        final Project project = projectService.removeByIndex(index);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT BY ID]");

        System.out.print("ENTER ID: ");
        final String id = TerminalUtil.nextLine();
        System.out.print("ENTER NEW NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();

        final Project project = projectService.updateById(id, name, description);
        if (project == null) return;

        System.out.println("UPDATED PROJECT - " + project.toString());
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT BY INDEX]");

        System.out.print("ENTER INDEX: ");
        final Integer index = TerminalUtil.nextInteger() - 1;
        System.out.print("ENTER NEW NAME: ");
        final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW DESCRIPTION: ");
        final String description = TerminalUtil.nextLine();

        final Project project = projectService.updateByIndex(index, name, description);
        if (project == null) return;

        System.out.println("UPDATED PROJECT - " + project.toString());
        System.out.println("[OK]");
    }

}